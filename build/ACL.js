// @author laudeon
// @license MIT
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _boom = _interopRequireDefault(require("@hapi/boom"));

var _debug = _interopRequireDefault(require("debug"));

var _throwIfMissing = _interopRequireDefault(require("throw-if-missing"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const DEFAULT_GROUP = 'user';
const debug = (0, _debug.default)('durassell:acl'); // Expose a simple ACL middleware
// Made for expressjs
// You should expose the current authed user in a locals.user object
// @see https://expressjs.com/en/4x/api.html#app.locals on the app.locals object

class ACL {
  constructor(rules = (0, _throwIfMissing.default)`acl rules`) {
    this.rules = rules;
  }

  _parseRequest(req) {
    let requestBaseURL = '';

    if (req.route.path !== '/') {
      requestBaseURL = req.baseUrl + req.route.path;
    } else {
      requestBaseURL = req.baseUrl;
    }

    return requestBaseURL;
  }

  _checkOwn(token, locals, localsUserIdProperty) {
    return localsUserIdProperty.split('.') // for instance: ['user', 'id'], depending on localUserIdroperty given in rule
    .reduce((acc, v) => acc && acc[v] || null, locals) // First, returns locals.user, then user.id
    == token.uid;
  }

  _isAuthorized(token, locals, req) {
    let matchedgroup, matchedBaseUrl;
    req.baseUrl = this._parseRequest(req);

    if (!token.ugroup) {
      debug('acl:no group found in token');
      return false;
    }

    if (!this.rules.hasOwnProperty(token.ugroup)) {
      debug('acl:no group matched in rules for the given token ugroup');
      return false;
    }

    matchedgroup = this.rules[token.ugroup];

    if (!matchedgroup.hasOwnProperty(req.baseUrl)) {
      debug('acl:no path matched in the defined rules with the given token ugroup');
      return false;
    }

    matchedBaseUrl = matchedgroup[req.baseUrl];

    if (matchedBaseUrl.hasOwnProperty('methods') && matchedBaseUrl.methods.includes(req.method)) {
      return true;
    }

    if (matchedBaseUrl.hasOwnProperty('restrictToOwn') && matchedBaseUrl.restrictToOwn.hasOwnProperty('methods') && matchedBaseUrl.restrictToOwn.methods.includes(req.method)) {
      return this._checkOwn(token, locals, matchedBaseUrl.restrictToOwn.localsUserIdProperty);
    }

    debug('acl:no method matched');
    return false;
  }

  middleware(req, res, next) {
    const locals = res.locals;
    let token = locals.token;
    if (!token) token = {
      ugroup: DEFAULT_GROUP,
      uid: null
    };

    if (!this._isAuthorized(token, locals, req)) {
      debug('unauthorized', token);
      return next(_boom.default.unauthorized(`for ${req.baseUrl}`));
    }

    return next();
  }

}

exports.default = ACL;