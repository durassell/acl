// @flow
// @author laudeon
// @license MIT

'use strict'

import Boom from '@hapi/boom'
import Debug from 'debug'
import x from 'throw-if-missing'

const DEFAULT_GROUP: string = 'user'

const debug = Debug('durassell:acl')

// Expose a simple ACL middleware
// Made for expressjs
// You should expose the current authed user in a locals.user object
// @see https://expressjs.com/en/4x/api.html#app.locals on the app.locals object
export default class ACL {
  rules: Object

  constructor (rules: Object = x`acl rules`): void {
    this.rules = rules
  }

  _parseRequest (req: express$Request) {
    let requestBaseURL = ''
    if (req.route.path !== '/') {
      requestBaseURL = req.baseUrl + req.route.path
    } else {
      requestBaseURL = req.baseUrl
    }

    return requestBaseURL
  }

  _checkOwn (token: Object, locals: Object, localsUserIdProperty: String) {
    return localsUserIdProperty
            .split('.') // for instance: ['user', 'id'], depending on localUserIdroperty given in rule
            .reduce((acc, v) => acc && acc[v] || null, locals)  // First, returns locals.user, then user.id
            == token.uid
  }
  
  _isAuthorized (token: Object, locals: Object, req: express$Request): boolean {
    let matchedgroup, matchedBaseUrl
    req.baseUrl = this._parseRequest(req)

    if (!token.ugroup) {
      debug('acl:no group found in token')
      return false
    }
    if (!this.rules.hasOwnProperty(token.ugroup)) {
      debug('acl:no group matched in rules for the given token ugroup')
      return false
    }
    matchedgroup = this.rules[token.ugroup]

    if (!matchedgroup.hasOwnProperty(req.baseUrl)) {
      debug('acl:no path matched in the defined rules with the given token ugroup')
      return false
    }
    matchedBaseUrl = matchedgroup[req.baseUrl]

    if (
      matchedBaseUrl.hasOwnProperty('methods') && 
      matchedBaseUrl.methods.includes(req.method)
    ) {
      return true
    }

    if (
      matchedBaseUrl.hasOwnProperty('restrictToOwn') &&
      matchedBaseUrl.restrictToOwn.hasOwnProperty('methods') &&
      matchedBaseUrl.restrictToOwn.methods.includes(req.method)
    ) {
      return this._checkOwn(token, locals, matchedBaseUrl.restrictToOwn.localsUserIdProperty)
    }

    debug('acl:no method matched')
    return false
  }

  middleware (
    req: express$Request, 
    res: express$Response, 
    next: express$NextFunction
  ): mixed {
    const locals = ((res.locals: any): {[string]: string})
    let token = ((locals.token: any): {[string]: string})
  
    if (!token) token = { ugroup: DEFAULT_GROUP, uid: null }
  
    if (!this._isAuthorized(token, locals, req)) {
      debug('unauthorized', token)
      return next(Boom.unauthorized(`for ${req.baseUrl}`))
    }
  
    return next()
  }
}
