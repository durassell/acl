// @author laudeon
// @license MIT

'use strict'

import ACL from './ACL'
import boom from '@hapi/boom'

let req
let res
let next = jest.fn()
let jsonFunc = jest.fn()

const acl = new ACL({
  "admin": {
    "/v1/users": {
      "methods": ["GET"],
    },
    "/v1/users/:userId": {
      "methods": ["GET"],
      "restrictToOwn": {
        "methods": ["PUT", "PATCH", "DELETE"],
        "localsUserIdProperty": "user.id"
      }
    }
  },
  "guest": {
    "/v1/users": {
      "methods": ["GET"],
    },
    "/v1/users/:userId": {
      "restrictToOwn": {
        "methods": ["GET"],
        "localsUserIdProperty": "user.id"
      }
    }
  }
})

beforeEach(() => {
  req = {
    body: {},
    params: {},
    baseUrl: '',
    method: 'GET',
    route: {}
  }
  res = {
    status: jest.fn(() => {
      return {
        json: jsonFunc
      }
    }),
    locals: {
      user: {
        id: ''
      }
    },
  }
})

test('rules', () => {
  expect(acl.rules).toHaveProperty("guest")
  expect(acl.rules).toHaveProperty("admin")
})


describe('_isAuthorize', () => {
  describe('No group', () => {
    test('Should return false if group is not set', () => {
      const token = { ugroup: '', uid: 1 }
      req.params.id = 1
      req.baseUrl = '/v1/users'
      req.route.path = ''

      const isAuthorize = acl._isAuthorized(token, res.locals, req)

      expect(isAuthorize).toBe(false)
    })
  })

  describe('Not allowed for guest', () => {
    test('Should return false, route acces is restricted to the admin group', () => {
      const token = { ugroup: 'guest', uid: 1 }
      req.baseUrl = '/v1/users'
      req.route.path = ''
      req.method = 'POST'

      const isAuthorize = acl._isAuthorized(token, res.locals, req)

      expect(isAuthorize).toBe(false)
    })
  })

  describe('Not allowd on own resource', () => {
    test('Should return false, user id does not match resource author id', () => {
      const token = { ugroup: 'admin', uid: 1 }
      req.method = "PUT"
      res.locals.user.id = 1234
      req.baseUrl = '/v1/users'
      req.route.path = '/:userId'
      req.params['userId'] = 1234

      const isAuthorize = acl._isAuthorized(token, res.locals, req)

      expect(isAuthorize).toBe(false)
    })
  })

  describe('Allowd on self', () => {
    test('Should return true, user id does match resource author id', () => {
      const token = { ugroup: 'admin', uid: 1234 }
      res.locals.user.id = 1234
      req.baseUrl = '/v1/users'
      req.route.path = '/:userId'
      req.params['userId'] = 1234

      const isAuthorize = acl._isAuthorized(token, res.locals, req)

      expect(isAuthorize).toBe(true)
    })
  })

  describe('Allowed', () => {
    test('Should return true, user group is admin', () => {
      const token = { ugroup: 'admin', uid: 1 }
      req.baseUrl = '/v1/users'
      req.route.path = ''

      const isAuthorize = acl._isAuthorized(token, res.locals, req)

      expect(isAuthorize).toBe(true)
    })
  })
})

describe('Middleare', () => {
  describe('Happy path', () => {
    test('Should call _isAuthorized', () => {
      acl._isAuthorized = jest.fn()

      acl.middleware(req, res, next)
      expect(acl._isAuthorized).toHaveBeenCalled()
    })

    test('Should call next on authorized', () => {
      acl._isAuthorized = jest.fn(() => true)
      
      acl.middleware(req, res, next)
      expect(next).toHaveBeenCalledWith()
    })
  })

  describe('Unauthorized', () => {
    test('Should call next with boom unauthorized error on unauthorized', () => {
      acl._isAuthorized = jest.fn(() => false)

      acl.middleware(req, res, next)
      expect(next).toHaveBeenCalledWith(boom.unauthorized('for '))
    })
  })
})
